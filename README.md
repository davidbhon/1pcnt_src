# 1pcnt_src

Alt 1percent.us src -- mostly c/c++ with some bash and python

https://gitlab.com/davidbhon/1pcnt_src.git

Note this is separate from my other 1pcnt_src repo (mostly python): https://gitlab.com/1percent1/1pcnt_src.git

Lots of projects to consider url = https://github.com/fffaraz/awesome-cpp#networking

First choice? https://github.com/uNetworking/uWebSockets.git

Allegedly simple server and client header only, supports GET and more url = https://github.com/yhirose/cpp-httplib.git

Another allegedly Simmple c++ webserver url = https://github.com/etr/libhttpserver.git

C++ python-like requests url = https://github.com/libcpr/cpr.git

Asynchronous-Programming-in-CPP	url = https://github.com/PacktPublishing/Asynchronous-Programming-in-CPP.git

C-23-STL-Cookbook-Second-Edition url = https://github.com/PacktPublishing/C-23-STL-Cookbook-Second-Edition.git

cpp20-http-client url = https://github.com/avocadoboi/cpp20-http-client.git

CPP-20-STL-Cookbook url = https://github.com/PacktPublishing/CPP-20-STL-Cookbook.git

cpp-httplib url = https://github.com/yhirose/cpp-httplib.git

curlpp url = https://github.com/jpbarrette/curlpp.git

http-server url = https://github.com/OsasAzamegbe/http-server.git

Modern-Cpp-Programming-Cookbook-Third-Edition url = https://github.com/PacktPublishing/

paraview-superbuild url = https://gitlab.kitware.com/paraview/paraview-superbuild.git

paraview-superbuild url = https://gitlab.kitware.com/paraview/common-superbuild.git

poco url = https://github.com/pocoproject/poco.git -- c++ resource for serves and client app dev

