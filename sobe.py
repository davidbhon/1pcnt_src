#!/usr/bin/env python3
"""
2024 budget and 2025 extrap
"""
sobe = 4278./12 # prop tax
sobe += 640 # hoa
sobe += 255.5 # utils
cars = 358.0 + 195.0 # loan + insurance
exp = sobe+cars
food = 55*31
exp += food
exp25 = exp - 458 # 2025 sell nissan and keep jeep?

ss = ss2024 = 1638.0 # social security monthly after withholdings
ss2025 = 1.04*ss2024 ; print(ss,ss2025)
ml = 992./3
fid = 27.0/63.5 * ml
z = 5205.75/9.3
# avg monthly div inc so far this year as of oct 8 
div = 0.8*(z+fid+ml) # although some z inc is tax free munis
inc = ss + div
net = inc - exp
inc25 = div + ss2025
net25 = inc25 - exp25
print('2024:', inc,-exp,'==', net)
print('2025:', inc25,-exp25,'==', net25)

rent_sobe = 2300.0*6.0/12.0 # approx monthly income if 6 month rental is possible
rnet = net + rent_sobe

print(net,rnet)

