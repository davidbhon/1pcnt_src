//#include <fstream>
#include <iostream>
#include <string>

//#define CPPHTTPLIB_OPENSSL_SUPPORT
#include "httplib.h"

int main(int argc, char * argv[]) {
  std::string hello = "Hello World! ... \n";
  std::string mimetyp = "text/html"; // "text/plain";
  // HTTP
  httplib::Server svr;
  svr.Get("/hi", [](const httplib::Request &, httplib::Response &res) {
    //res.set_content(hello, mimetyp); // evidently compiler dislikes this
    res.set_content("Hello World! ... \n", "text/html");
  });
  svr.listen("0.0.0.0", 8080);

  // HTTPS
  //httplib::SSLServer sslsvr;
  //sslsvr.Get("/hi", [](const httplib::Request &, httplib::Response &res) {
  // res.set_content(hello, mimetyp);
  //});
  //sslsvr.listen("0.0.0.0", 8443);
}

