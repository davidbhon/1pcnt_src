#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>

//
// gcc -static -ggdb -Wall -Wextra -Wpedantic -o ghbn ghbn.c
//

int main(int argc, char * argv[]) {
  char* hostxt = (char*) "localhost";
  if( argc >= 2 ) hostxt = argv[1];
  struct hostent* hosts = gethostbyname(hostxt);
  if( ! hosts ) {
    printf("host name not found: %s\n", hostxt);
    exit(1);
  }
  struct in_addr* address = (struct in_addr*) hosts->h_addr;
  const char* name = inet_ntoa(*address);
  if( name ) {
    printf("%s\n", name);
  } else {
    printf("host IP not found: %s\n", hostxt);
  }
}
