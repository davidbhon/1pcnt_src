
//#include <fstream>
#include <iostream>
#include <string>

//#define CPPHTTPLIB_OPENSSL_SUPPORT 1
#include <httplib.h>

std::string& get_https(std::string& url, std::string& html) {
  std::clog << url << std::endl;
  //httplib::SSLClient sslcli(url, 443)
  //sslcli.set_follow_location(true);
  //sslcli.enable_server_certificate_verification(false);
  //sslcli.enable_server_certificate_verification(true);
  //sslcli.get_openssl_verify_result()
  //httplib::Result ressl = sslcli.get_openssl_result();
  //if (ressl && ressl->status == 200) {
  //  std::clog << ressl->body << std::endl;
  //}
  //else {
  //  auto err = ressl.error();
  //  std::clog << httplib::to_string(err) << std::endl;
  //}
  //std::clog << url << std::endl;
  html = "sorry https needs more work";
  return html;
}

std::string& savefile(std::string& url, std::string& filename, std::string& html) {
  //https://cplusplus.com/doc/tutorial/files/
  size_t idx = 3 + url.rfind(":");
  filename = url.substr(idx)+".html";
  std::clog << "save content fetch result to file: " << filename << std::endl;
  try {
    std::ofstream ofile(filename, std::ios::out);
    ofile << html << std::endl;
    ofile.close();
  } catch(...) {
    std::clog << "failed to open/write html to: " << filename << std::endl;
  }
  // In P0849R8, C++23 added “auto decay copy” as a core language feature, so starting in C++23,
  // use auto(x) to make a copy of x, removing the need for the copy_of function
  //
  // auto should return deep copy of filename which is presumably in/on this function staek and ephemeral...
  // alas auto dies not compile?
  return filename; // auto(filename);
}

std::string& get_http(std::string& url, std::string& html) {
  std::clog << url << std::endl;
  httplib::Client cli(url);
  cli.set_follow_location(true);
  httplib::Result res = cli.Get("/");
  if (res && res->status == 200) {
    html = res->body;
  }
  else {
    auto err = res.error();
    html = httplib::to_string(err);
  }
  return html;
}

int main(int argc, char * argv[]) {
  std::string filename = "downloaded_file";
  std::string url = "http://www.google.com";
  if( argc >= 2 ) url = argv[1];

  std::string html("html");
  html = get_http(url, html);
  filename = savefile(url, filename, html); // possible filename rename?
  std::clog << "seved file: " << filename << std::endl;

  //html = get_https(url);
  //auto filename = savefile(url, html);
  //std::clog << "saved content fetch result to file: " << filename << std::endl;
}

