#!/usr/bin/env python3
import sys
from copy import deepcopy as dpcpy
from urllib import request as urlreq
import requests

_headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'
}

#_debug = False # True
_debug = True # False

_mwfund_url = 'https://www.marketwatch.com/investing/fund/'
_tltw = _mwfund_url + 'tltw'

def welcome_html(text=None):
  """
  <html>
  <head>
  <title>Title Hello</title>
  </head>
  <body>
  <h1>Hello World</h1>
  <p>This is a paragraph of google AI generated code...</p>
  </body>
  </html>
  """
  # help(welcome_html)
  hello = welcome_html.__doc__ 
  if(text):
    hello = '<html><head><title>Title Hello</title></head>'
    hello += '<body><h1>Hello World</h1>' + '<p>' + text + '</p>' + '</body></html>'
  return hello

def pprint(html=None):
  if(_debug): html = welcome_html()
  if not(html):
    print("sorry no HTML provided...")
    return None
  try:
    import html5lib
    htext = html5lib.parse(html)
    print(htext)
  except:
    print("sorry need valid HTML5")
    return None
  return htext

def BSprint(html=None):
  """
  use beautiful soup to pretty-print html page
  """
  from bs4 import BeautifulSoup as BS
  if not(html): html = welcome_htm() 
  if(_debug): print(html)
  soup = BS(html)
  bs = soup.prettify() 
  if(bs):
    print(bs) 
  else:
    print("sorry pprint of BS seems faulty", ':)')
  return bs
# end pprint()

def fetch_quote(quote_url):
  """
  use urllib to get current quotes as utf-8
  optionally print some html content
  """
  global _debug
  html = resp = None
  try:
    tmo = 5
    if(_debug): print("attempt to fetch quote fromn nasdaq", quote_url,"... using timeout:", tmo)
    resp = requests.get(quote_url, headers=_headers, timeout=tmo)
    html = resp.text
    # resp = urlreq.urlopen(quote_url, timeout=tmo)
    # if(resp): html = resp.read().decode('utf-8')  # Decode the response (usually UTF-8)
    if(_debug and html): pprint(html)
  except: print("sorry site will not respond:", quote_url, '\n')
  return(html)
# end fetch_quote()

def mktwatch(tickr_list=['qlte', 'rdte', 'xdte', 'spyi', 'tltw']):
  global _debug ; global _mwfund_url
  quote_dict = {} ; curval = None
  for tickr in tickr_list:
    curval = None
    if not(tickr): # skip?
      print("bummer bad tickr: " + repl(tickr), "... skip ...")
      continue # next foor loop item
    quote_url = _mwfund_url + tickr
    curval = fetch_quote(quote_url)
    if not(curval): # insert sumthin into dict...
      curval = "bummer, market watch has failed for tickr: " + tickr
    else:
      quote_dict[tickr] = curval
    if(_debug): print(curval)
  return quote_dict
# end mktwatch() 

def nasdaq(tickr_list=['qlte', 'rdte', 'xdte', 'spyi', 'tltw']):
  """
  example results circa 10/03/2024
  https://www.nasdaq.com/market-activity/etf/tltw/dividend-history
  https://www.nasdaq.com/market-activity/etf/qdte/dividend-history
  https://www.nasdaq.com/market-activity/etf/xdte/dividend-history
  https://www.nasdaq.com/market-activity/etf/rdte/dividend-history
  qlte = 0.21 * 52/42.5 # 52 weekly
  xdte = 0.228 * 52/52.3 #
  rdte = (0.386+0.360)/2.0 # 2 weekly
  spyi = 6.1764 / 12.0 / 51.20 # monthly div ... annual yield-ish 12.06%
  tltw = 2.491 / 12.0 # monthly ... annual yield-ish 9.52%
  """
  global _debug
  quote_dict = {} ; curval = None
  for tickr in tickr_list:
    curval = None
    if not(tickr): # skip?
      curval = "bummer bad tickr: " + repl(tickr)
      contiue # next foor loop item
    quote_url = 'https://www.nasdaq.com/market-activity/etf/' + tickr + '/dividend-history'
    curval = fetch_quote(quote_url)
    if not(curval): # insert sumthin into dict...
      curval = "bummer, nasdaq has failed for tickr: " + tickr
    quote_dict[tickr] = curval
    if(_debug): print(curval)
  return quote_dict
# end nasdaq() 

def main(argc=0, args=['qlte', 'rdte', 'xdte', 'spyi', 'tltw']):
  if not(args and argc): args=['qlte', 'rdte', 'xdte', 'spyi', 'tltw']
  print(args)
  #qdict = nasdaq(args);
  qdict = mktwatch(args);
  pprint(qdict)
  return len(qdict)

if __name__ == "__main__":
  args = None
  if( len(sys.argv) > 1 ):
    args = dpcpy(sys.argv[1:])
    argc = len(args)
  else:
    argc = 0

  cnt = main(argc, args)
# all done
