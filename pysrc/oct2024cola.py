#!/usr/bin/env python3

ss = 1639.0 # after federal tax witheld and medicare

mlinc = 0.8 * (3937.85/12.0)
fedtax = 0.2 * (3937.85/12.0)
0.0 # ss inc is below poverty, but assume IRA witholding of 20%
fidinc = 0.8 * (661.0/3.0)
fedtax += 0.2 * (661.0/3.0)

irainc = mlinc + fidinc
inc = irainc + ss
print("IRA and Social Security total inc after ALL witholdings:", inc)
print("federal taxes:", fedtax)

proptax = 4365.13 / 12.0
motax = proptax + fedtax
hoa = 645.0
util = 250.0
print("hoa, utils, property tax:", hoa, util, proptax)

jeep = 150.0 # should be less than nissan?
nissan = 357.5 + 150.0
print("jeep, nissan:", jeep, nissan)

cars = jeep + nissan  # jeep and rogue monthly ?
exptot = motax + hoa + util + cars
print("min monthly expenses -- no food:", exptot)

netinc = inc - exptot
print("net monthly inc:", netinc)

dayinc = netinc / 31.0
print("net / day inc:", dayinc)


